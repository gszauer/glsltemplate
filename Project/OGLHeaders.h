#ifndef _H_OGLHEADERS_
#define _H_OGLHEADERS_

#if __APPLE__
    #include <OpenGL/gl.h>
    #include <GLUT/glut.h>
    #include <OpenGL/glu.h>
#elif __linux
    #include <GL/glew.h>
    #include <GL/gl.h>
    #include <GL/glut.h>
#elif _WIN32
    #include <Windows.h>
    #include <gl/GL.h>
    #include <gl/GLU.h>
    #include <GL/openglut.h>
#else
    #error Include OpenGL Headers
#endif

#include <sys/time.h>
#include <cstdlib>
#include <string>
#include <iostream>
#include <fstream>
#include <unistd.h>
#include <vector>
#include <glm/glm.hpp>
#include <glm/ext.hpp>

#include <mach-o/dyld.h>
#include <limits.h>

inline std::string GetEecutablePath() {
#if __linux
    char buff[1024];
    ssize_t len = ::readlink("/proc/self/exe", buff, sizeof(buff)-1);
    if (len != -1) {
        buff[len] = '\0';
        
        std::string::size_type pos = std::string( buff ).find_last_of( "\\/" );
        return std::string( buff ).substr( 0, pos) + "/";
    }
#elif __APPLE__
    char buf [PATH_MAX];
    uint32_t bufsize = PATH_MAX;
    if(!_NSGetExecutablePath(buf, &bufsize)) {
        std::string::size_type pos = std::string( buf ).find_last_of( "\\/" );
        return std::string( buf ).substr( 0, pos) + "/";
    }
#endif
    return "null";
}

inline double GetMilliseconds() {
    static timeval s_tTimeVal;
	gettimeofday(&s_tTimeVal, NULL);
	double time = s_tTimeVal.tv_sec * 1000.0; // sec to ms
	time += s_tTimeVal.tv_usec / 1000.0; // us to ms
	return time;
}

inline int CompileShader(const char* shader, unsigned int type) {
    GLuint result = glCreateShader((GLenum)type);
    GLint status;
    if (result == 0) {
        std::cout << "Could not create shader!\n";
        return -1;
    }
    int shader_length = int(strlen(shader));
    glShaderSource(result, 1, (const GLchar**)&shader, &shader_length);
    glCompileShader(result);
    glGetShaderiv(result, GL_COMPILE_STATUS, &status);
    if (status == GL_FALSE) {
        std::cout << "Could not compile (camera) shader!\n";
        glGetShaderiv(result, GL_INFO_LOG_LENGTH, &status);
        if (status > 0) {
            char* log = new char[status];
            glGetShaderInfoLog(result, status, &status, log);
            std::cout << "\t: " << log << "!\n";
            delete[] log;
        }
        return -1;
    }
    
    return result;
}

inline void PrintGLError() {
    GLenum errCode;
    
    if ((errCode = glGetError()) != GL_NO_ERROR) {
        std::cout << "OpenGL error #" << errCode << ": " << gluErrorString(errCode) << "\n";
    }
    assert(errCode == GL_NO_ERROR);
}

inline void PrintFBOStatus(GLenum status) {
    const char *err_str = 0;
    char buf[80];
    
    if ( status != GL_FRAMEBUFFER_COMPLETE )
    {
        switch ( status )
        {
            case GL_FRAMEBUFFER_UNSUPPORTED:
                err_str = "UNSUPPORTED";
                break;
            case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
                err_str = "INCOMPLETE ATTACHMENT";
                break;
            case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER:
                err_str = "INCOMPLETE DRAW BUFFER";
                break;
            case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER:
                err_str = "INCOMPLETE READ BUFFER";
                break;
            case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
                err_str = "INCOMPLETE MISSING ATTACHMENT";
                break;
            case GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE:
                err_str = "INCOMPLETE MULTISAMPLE";
                break;
                /*case GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS:
                 err_str = "INCOMPLETE LAYER TARGETS";
                 break;
                 */
                // Removed in version #117 of the EXT_framebuffer_object spec
                //case GL_FRAMEBUFFER_INCOMPLETE_DUPLICATE_ATTACHMENT:
                
            default:
                sprintf( buf, "0x%x", status );
                err_str = buf ;
                break;
        }
        
        printf( "ERROR: glCheckFramebufferStatus() returned '%s'", err_str );
    }
}

inline glm::quat lerp(const glm::quat& from, const glm::quat& to, float delta) {
    return (from * (1.0f - delta)) + (to * delta);
}

inline glm::quat slerp(const glm::quat& q1, const glm::quat& q2, float t) {
    glm::quat q3;
    float dot = q1.x*q2.x + q1.y*q2.y+q1.z*q2.z+q1.w*q2.w;
    
    if (dot < 0) {
        dot = -dot;
        q3 = q2 * -1.0f;
    }
    else {
        q3 = q2;
    }
    
    if (dot < 0.95f) {
        float angle = acosf(dot);
        return (q1*sinf(angle*(1-t))) + (q3*sinf(angle*t)) * (1.0f /sinf(angle));
    }
    else { // if the angle is small, use linear interpolation
        return lerp(q1,q3,t);
    }
}

#endif