#include "Camera.h"
#include "Texture.h"
#include "OGLHeaders.h"
#include <assert.h>
#include <iostream>
#include <OpenGL/glu.h>
#include <sstream>
#include "OGLHeaders.h"

// Render code: https://bitbucket.org/gszauer/tightfittingobbvolume/src/cf0bdc17e8f601a71b437d4af49df531aa73f1f5/Source/Camera.cpp?at=master
// Also:        https://gist.github.com/gszauer/7941306
// And:         http://www.gamedev.net/topic/651106-world-space-camera-frustum/

void Camera::Destroy() {
    assert(m_nFramebufferId != 0);
    assert(m_nVBO != 0);
    assert((m_eRenderMode == COLOR_DEPTH)? m_pRenderTarget != 0  : 1);
    assert(m_pDepthTarget != 0);
    
    if (m_pDepthTarget != 0)
        delete m_pDepthTarget;
    if (m_pRenderTarget != 0)
        delete m_pRenderTarget;
    if (m_nFramebufferId != 0)
        glDeleteFramebuffersEXT(1, &m_nFramebufferId);
    if (m_nVBO != 0)
        glDeleteBuffers(1, &m_nVBO);
    
    m_pDepthTarget = 0;
    m_pRenderTarget = 0;
    m_nFramebufferId = 0;
    m_nVBO = 0;
}

void Camera::CreateDepthTarget(int w, int h) {
    assert(m_pDepthTarget == 0);
    assert(w > 0 && h > 0);
    m_pDepthTarget = new Texture(w, h, Texture::DEPTH);
}

void Camera::CreateRenderTarget(int w, int h) {
    assert(m_pRenderTarget == 0);
    assert(w > 0 && h > 0);
    m_pRenderTarget = new Texture(w, h, Texture::RGBA_24);
}

void Camera::CreateFramebuffer() {
    assert(m_nFramebufferId == 0);
    assert((m_eRenderMode == COLOR_DEPTH)? m_pRenderTarget != 0  : 1);
    assert(m_pDepthTarget != 0);
    
    // Create the frame buffer object
    glGenFramebuffersEXT(1, &m_nFramebufferId);
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, m_nFramebufferId);
    
    if (m_eRenderMode == DEPTH) {
        // Instruct openGL that we won't bind a color texture with the currently bound FBO
        glDrawBuffer(GL_NONE); // Depth will not render to other buffers!
        glReadBuffer(GL_NONE); // Depth will not render to other buffers!
        
        // attach the texture to FBO depth attachment point
        glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT,GL_TEXTURE_2D, m_pDepthTarget->GetHandle(), 0);
    }
    else { // Depth and color
        glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, m_pRenderTarget->GetHandle(), 0);
        glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT,GL_TEXTURE_2D, m_pDepthTarget->GetHandle(), 0);
        //TODO: glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_STENCIL_ATTACHMENT_EXT, GL_TEXTURE_2D , m_pDepthTarget->GetHandle(), 0);
    }
    
    // check FBO status
    GLenum fboStatus = glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT);
    if(fboStatus != GL_FRAMEBUFFER_COMPLETE_EXT) {
        std::cout << "GL_FRAMEBUFFER_COMPLETE_EXT failed, CANNOT use FBO\n";
        PrintFBOStatus(fboStatus);
    }
    assert(fboStatus == GL_FRAMEBUFFER_COMPLETE_EXT);
    
    // switch back to window-system-provided framebuffer
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
    //glDrawBuffer(GL_BACK); // Draw to back buffer
    //glReadBuffer(GL_BACK); // Read from back buffer
}

std::vector<glm::vec3> Camera::GetWorldSpaceCorners() {
    glm::mat4 viewProjMatrix = m_matProj * m_matView;
    glm::mat4 inverseProjectionMatrix = glm::inverse(viewProjMatrix);
    
    std::vector<glm::vec3> corners; corners.reserve(8);
    for (unsigned x = 0; x < 2; x++) {
        for (unsigned y = 0; y < 2; y++){
            for (unsigned z = 0; z < 2; z++) { // I might mess up the order of the frustrum corners here. But the position should be correct
                glm::vec4 projClipSpacePosition(x*2.0f-1.0f, y*2.0f-1.0f, z*2.0f-1.0f, 1.0f);
                glm::vec4 projWorldSpacePosition = inverseProjectionMatrix * projClipSpacePosition;
                corners.push_back(glm::vec3(projWorldSpacePosition.x / projWorldSpacePosition.w,
                                            projWorldSpacePosition.y / projWorldSpacePosition.w,
                                            projWorldSpacePosition.z / projWorldSpacePosition.w
                                            ));
            }
        }
    }
    
    return corners;
}

void Camera::CreateRenderVBO() {
    assert(m_nVBO == 0);
    
    std::vector<glm::vec3> corners = GetWorldSpaceCorners();
    unsigned int render[] = {1,3,1,5,7,3,7,5,0,2,0,4,6,2,6,4,6,7,2,3,0,1,4,5};
    std::vector<glm::vec3> vertexList; vertexList.reserve(24);
    for (int i = 0; i < 24; ++i)
        vertexList.push_back(corners[render[i]]);
    
    assert(vertexList.size() > 0);
    glGenBuffers(1, &m_nVBO);
    glBindBuffer(GL_ARRAY_BUFFER, m_nVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * vertexList.size(), &vertexList[0].x, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void Camera::UpdateRenderVBO() {
    assert(m_nVBO != 0);
    
    std::vector<glm::vec3> corners = GetWorldSpaceCorners();
    unsigned int render[] = {1,3,1,5,7,3,7,5,0,2,0,4,6,2,6,4,6,7,2,3,0,1,4,5};
    std::vector<glm::vec3> vertexList; vertexList.reserve(24);
    for (int i = 0; i < 24; ++i)
        vertexList.push_back(corners[render[i]]);
    
    assert(vertexList.size() > 0);
    glBindBuffer(GL_ARRAY_BUFFER, m_nVBO);
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(glm::vec3) * vertexList.size(), &vertexList[0].x);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

Camera::Camera(int w, int h, float fov, float aspect, float _near, float _far) {
    m_nScreenW = w;
    m_nScreenH = h;
    
    int u2 = 1;
    int v2 = 1;
    while(u2 < w) u2 *= 2;
    while(v2 < h) v2 *= 2;
    w = u2;
    h = v2;
    
    m_nNearPlane = _near;
    m_nFarPlane = _far;
    
    m_nIsDepthUniform = 0;
    m_nClipPlanesUniform = 0;
    m_nBlitShader = 0;
    m_nUvAttrib = 0;
    m_nMVPUniform = 0;
    m_nPositionAttrib = 0;
    m_nTextureUniform = 0;
    m_nScaleUniform = 0;
    m_nScreenSpaceVBO = 0;
    InitBlitShader();
    
    m_pRenderTarget = 0;
    m_pDepthTarget = 0;
    m_nFramebufferId = 0;
    m_nVBO = 0;
    
    CreateRenderTarget(w, h);
    CreateDepthTarget(w, h);
    CreateFramebuffer();
    CreateRenderVBO();
    
    m_eRenderMode = COLOR_DEPTH;
    m_vecPosition = glm::vec3(0.0f, 0.0f, 0.0f);
    m_quatRotation = glm::quat();
    m_matView = glm::mat4();
    m_matProj = glm::perspective(fov, aspect, _near, _far);
    UpdateRenderVBO();
}

Camera::Camera(int w, int h, RenderMode mode, float fov, float aspect, float _near, float _far) {
    m_nScreenW = w;
    m_nScreenH = h;
    
    int u2 = 1;
    int v2 = 1;
    while(u2 < w) u2 *= 2;
    while(v2 < h) v2 *= 2;
    w = u2;
    h = v2;
    
    m_nNearPlane = _near;
    m_nFarPlane = _far;
    
    m_nIsDepthUniform = 0;
    m_nClipPlanesUniform = 0;
    m_nBlitShader = 0;
    m_nUvAttrib = 0;
    m_nMVPUniform = 0;
    m_nPositionAttrib = 0;
    m_nTextureUniform = 0;
    m_nScaleUniform = 0;
    m_nScreenSpaceVBO = 0;
    InitBlitShader();
    
    m_pRenderTarget = 0;
    m_pDepthTarget = 0;
    m_nFramebufferId = 0;
    m_nVBO = 0;
    m_eRenderMode = mode;
    
    if (mode == COLOR_DEPTH)
        CreateRenderTarget(w, h);
    CreateDepthTarget(w, h);
    
    CreateFramebuffer();
    CreateRenderVBO();
    
    
    m_vecPosition = glm::vec3(0.0f, 0.0f, 0.0f);
    m_quatRotation = glm::quat();
    m_matView = glm::mat4();
    m_matProj = glm::perspective(fov, aspect, _near, _far);
    UpdateRenderVBO();
}

Camera::~Camera() {
    Destroy();
    assert(m_nBlitShader != 0);
    assert(m_nScreenSpaceVBO != 0);
    glDeleteProgram(m_nBlitShader);
    glDeleteBuffers(1, &m_nScreenSpaceVBO);
}

void Camera::Perspective(float fov, float aspect, float _near, float _far) {
    m_nNearPlane = _near;
    m_nFarPlane = _far;
    m_matProj = glm::perspective(fov, aspect, _near, _far);
    UpdateRenderVBO();
}

void Camera::LookAt(const glm::vec3& position, const glm::vec3& center) {
    m_matView = glm::lookAt(position, center, glm::vec3(0.0f, 1.0f, 0.0f));
    UpdateRenderVBO();
    m_vecPosition = glm::vec3(-m_matView[3][0], -m_matView[3][1], -m_matView[3][2]);
    m_quatRotation = glm::quat_cast(glm::transpose(m_matView));
}

void Camera::LookAt(const glm::vec3& position, const glm::vec3& center, const glm::vec3& up) {
    m_matView = glm::lookAt(position, center, up);
    UpdateRenderVBO();
    m_vecPosition = glm::vec3(-m_matView[3][0], -m_matView[3][1], -m_matView[3][2]);
    m_quatRotation = glm::quat_cast(glm::transpose(m_matView));
}

unsigned int Camera::GetFramebufferHandle() {
    return m_nFramebufferId;
}

Camera::RenderMode Camera::GetRendermode() {
    return m_eRenderMode;
}

glm::mat4 Camera::GetView() {
    return m_matView;
}

glm::mat4 Camera::GetProjection() {
    return m_matProj;
}

glm::vec3 Camera::GetPosition() {
    return m_vecPosition;
}

glm::quat Camera::GetRotation() {
    return m_quatRotation;
}

Texture* Camera::GetRenderTarget() {
    return m_pRenderTarget;
}

Texture* Camera::GetDepthTarget() {
    return m_pDepthTarget;
}

void Camera::SetRendermode(RenderMode mode) {
    int w = m_nScreenW;
    int h = m_nScreenH;
    
    Destroy();
    if (mode == COLOR_DEPTH)
        CreateRenderTarget(w, h);
    CreateDepthTarget(w, h);
    
    CreateFramebuffer();
    CreateRenderVBO();
    UpdateRenderVBO();
    m_eRenderMode = mode;
}

void Camera::SaveToPng() {
    long millis = ((long)(GetMilliseconds() * 0.001));
    std::cout << "Saving camera to png.\n";
    
    std::ostringstream oss;
    oss << GetEecutablePath() << millis << "_color.png";
    
    if (GetEecutablePath() != "null" && m_eRenderMode == COLOR_DEPTH)
        GetRenderTarget()->SavePNG(oss.str().c_str(), GetNear(), GetFar());
    else
        std::cout << "Could not save image!\n";
    
    if (GetEecutablePath() != "null") {
        oss = std::ostringstream(); oss << GetEecutablePath() << millis << "_depth.png";
        GetDepthTarget()->SavePNG(oss.str().c_str(), GetNear(), GetFar());
    }
    else {
        std::cout << "Saved image (" << millis << ") to: " << GetEecutablePath() << "\n";
    }
}

void Camera::UpdateFrameBuffer(int screenW, int screenH) {
    m_nScreenW = screenW;
    m_nScreenH = screenH;
    
    int u2 = 1;
    int v2 = 1;
    while(u2 < screenW) u2 *= 2;
    while(v2 < screenH) v2 *= 2;
    screenW = u2;
    screenH = v2;
    
    Destroy();
    if (m_eRenderMode == COLOR_DEPTH)
        CreateRenderTarget(screenW, screenH);
    CreateDepthTarget(screenW, screenH);
    
    CreateFramebuffer();
    CreateRenderVBO();
    UpdateRenderVBO();
}

void Camera::Render(int position) {
    assert(m_nVBO != 0);
    
    glBindBuffer(GL_ARRAY_BUFFER, m_nVBO);
    
    glEnableVertexAttribArray(position);
    
    glVertexAttribPointer(position, 3, GL_FLOAT, GL_FALSE, 0, 0);
    
    glDrawArrays(GL_LINES, 0, 24);
    
    glDisableVertexAttribArray(position);
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void Camera::Bind() {
    assert(m_pDepthTarget != 0);
    assert(m_nFramebufferId != 0);
    
    glBindFramebuffer(GL_FRAMEBUFFER, m_nFramebufferId);
    if (m_pRenderTarget == 0) {
        //glDrawBuffer(GL_NONE);
        //glReadBuffer(GL_NONE);
        glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);  // Disable color mask, so we only write to depth
    }
}

void Camera::Unbind() {
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
    //glDrawBuffer(GL_BACK); // Draw to back buffer
    //glReadBuffer(GL_BACK); // Read from back buffer
}

void Camera::InitBlitShader() {
    assert(m_nBlitShader == 0);
    const char* v_shader = "#version 120 \n\
    uniform mat4 mvp;\
    uniform vec2 scale;\
    attribute vec2 uv;\
    attribute vec3 position;\
    varying vec2 texCoord;\
    void main() {\
        texCoord = uv * scale;\
        gl_Position = mvp * vec4(position, 1.0);\
    }";
    const char* f_shader = "#version 120 \n\
    varying vec2 texCoord;\
    uniform sampler2D texture;\
    uniform vec2 clipPlanes;\
    uniform bool isDepth;\
    void main() {\
        if (isDepth) {\
            float n = clipPlanes.x;\
            float f = clipPlanes.y;\
            float z = texture2D(texture, texCoord).x;\
            z = (2.0 * n) / (f + n - z * (f - n));\
            gl_FragColor = vec4(z,z,z,1.0);\
        }\
        else {\
            gl_FragColor = texture2D(texture, texCoord);\
        }\
    }\0";
    
    GLuint vertexShader = CompileShader(v_shader, GL_VERTEX_SHADER);
    GLuint fragmentShader = CompileShader(f_shader, GL_FRAGMENT_SHADER);
    assert(vertexShader != -1 && fragmentShader != -1);
    
    // Link actual shader
    m_nBlitShader = glCreateProgram();
    GLint result;
    glAttachShader(m_nBlitShader, vertexShader);
    glAttachShader(m_nBlitShader, fragmentShader);
    glLinkProgram(m_nBlitShader);
    glGetProgramiv(m_nBlitShader, GL_LINK_STATUS, &result);
    if (result == GL_FALSE) {
        std::cout << "Could not link shader program!\n";
        glGetProgramiv(m_nBlitShader, GL_INFO_LOG_LENGTH, &result);
        if (result > 0) {
            char* log = new char[result];
            glGetProgramInfoLog(m_nBlitShader, result, &result, log);
            std::cout << "\t: " << log << "\n";
            delete[] log;
        }
    }
    
    assert(m_nBlitShader != -1);
    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);
    
    glUseProgram(m_nBlitShader);
    m_nUvAttrib = glGetAttribLocation(m_nBlitShader, "uv");
    m_nPositionAttrib = glGetAttribLocation(m_nBlitShader, "position");
    m_nTextureUniform = glGetUniformLocation(m_nBlitShader, "texture");
    m_nIsDepthUniform = glGetUniformLocation(m_nBlitShader, "isDepth");
    m_nClipPlanesUniform = glGetUniformLocation(m_nBlitShader, "clipPlanes");
    m_nScaleUniform = glGetUniformLocation(m_nBlitShader, "scale");
    m_nMVPUniform = glGetUniformLocation(m_nBlitShader, "mvp");
    
    assert(m_nScreenSpaceVBO == 0);
    glm::vec3 verts[] = {
        glm::vec3(0.0f, 0.0f, 0.0f),
        glm::vec3(0.0f, 1.0f, 0.0f),
        glm::vec3(1.0f, 1.0f, 0.0f),
        glm::vec3(1.0f, 0.0f, 0.0f)};
    glm::vec2 texCoords[] = {
        glm::vec2(0.0f, 0.0f),
        glm::vec2(0.0f, 1.0f),
        glm::vec2(1.0f, 1.0f),
        glm::vec2(1.0f, 0.0f)};
    unsigned int indices[] = {2, 1, 0, 0, 3, 2};
    std::vector<float> vertexList; vertexList.reserve(30);
    for (int i = 0; i < 6; ++i) {
        vertexList.push_back(verts[indices[i]].x);
        vertexList.push_back(verts[indices[i]].y);
        vertexList.push_back(verts[indices[i]].z);
    }
    for (int i = 0; i < 6; ++i) {
        vertexList.push_back(texCoords[indices[i]].x);
        vertexList.push_back(texCoords[indices[i]].y);
    }
    
    assert(vertexList.size() > 0);
    glGenBuffers(1, &m_nScreenSpaceVBO);
    glBindBuffer(GL_ARRAY_BUFFER, m_nScreenSpaceVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * vertexList.size(), &vertexList[0], GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glUseProgram(0);
}

float Camera::GetNear() {
    return m_nNearPlane;
}

float Camera::GetFar() {
    return m_nFarPlane;
}

void Camera::BlitToScreen(bool depthBuffer) {
    assert((!depthBuffer) ? m_pRenderTarget != 0 : 1);
    assert(m_pDepthTarget != 0);
    assert(m_nBlitShader != 0);
    assert(m_nScreenSpaceVBO != 0);
    
    glDepthMask(GL_FALSE);
    glDisable(GL_DEPTH_TEST);
    glUseProgram(m_nBlitShader);
    
    glActiveTexture(GL_TEXTURE0);
    float sx = 1.0f;
    float sy = 1.0f;
    if (m_pRenderTarget != 0 && !depthBuffer) {
        glBindTexture(GL_TEXTURE_2D, m_pRenderTarget->GetHandle());
        sx = m_nScreenW / m_pRenderTarget->GetWidth();
        sy = m_nScreenH / m_pRenderTarget->GetHeight();
    }
    else {
        glBindTexture(GL_TEXTURE_2D, m_pDepthTarget->GetHandle());
        sx = m_nScreenW / m_pDepthTarget->GetWidth();
        sy = m_nScreenH / m_pDepthTarget->GetHeight();
    }
        
    glUniform1i(m_nTextureUniform, 0);
    glUniformMatrix4fv(m_nMVPUniform, 1, GL_FALSE, glm::value_ptr(glm::ortho(0.0f, 1.0f, 0.0f, 1.0f, -1.0f, 1.0f)));
    glUniform2f(m_nScaleUniform, sx, sy);
    glUniform2f(m_nClipPlanesUniform, m_nNearPlane, m_nFarPlane);
    glUniform1i(m_nIsDepthUniform, (int)depthBuffer);
    
    glBindBuffer(GL_ARRAY_BUFFER, m_nScreenSpaceVBO);
    
    glEnableVertexAttribArray(m_nPositionAttrib);
    glEnableVertexAttribArray(m_nUvAttrib);
    
    glVertexAttribPointer(m_nPositionAttrib, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glVertexAttribPointer(m_nUvAttrib, 2, GL_FLOAT, GL_FALSE, 0, (GLvoid*)(6 * 3 * sizeof(float)));
    
    glDrawArrays(GL_TRIANGLES, 0, 6);
    
    glDisableVertexAttribArray(m_nPositionAttrib);
    glDisableVertexAttribArray(m_nUvAttrib);
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, 0);
    
    glUseProgram(0);
    glEnable(GL_DEPTH_TEST);
    glDepthMask(GL_TRUE);
}