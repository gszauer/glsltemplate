#include <iostream>
#if __linux
    #include <GL/glew.h>
    #include <GL/gl.h>
    #include <sys/time.h>
#elif WIN32
    #include <GL/glew.h>
    #include <Windows.h>
    #include <tchar.h>
    #include <gl/gl.h>
#else
    #include <OpenGL/gl.h>
    #include <sys/time.h>
#endif

#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#include <assert.h>
#include <OpenGL/glu.h>

#define WIN_WIDTH 800
#define WIN_HEIGHT 600
#define FPS 30
#define SKIP_TICKS      (1000 / FPS)

extern bool Initialize();
extern void Resize(int w, int h);
extern void Render();
extern bool Update(float dt);
extern void Shutdown();
extern void OnKeyDown(unsigned int key, float dt);

bool g_bAppRunning = true;
bool g_bManualTimestep = false;

double GetMilliseconds() {
#if !WIN32
    static timeval s_tTimeVal;
	gettimeofday(&s_tTimeVal, NULL);
	double time = s_tTimeVal.tv_sec * 1000.0; // sec to ms
	time += s_tTimeVal.tv_usec / 1000.0; // us to ms
#else
	double time = (double)GetTickCount() * 0.001;
#endif
	return time;
}

#if _WIN32
int _tmain(int argc, _TCHAR* argv[]) {
#else
    int main(int argc, char *argv[]) {
#endif
        if (SDL_Init(SDL_INIT_VIDEO)) {
            std::cout << "Error! Could not initialize SDL!\n";
            return 1;
        }
        
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);
        SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
        SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
        
        SDL_Window* displayWindow = SDL_CreateWindow("GLSL Demo", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, WIN_WIDTH, WIN_HEIGHT, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
        SDL_GLContext openGlContext = SDL_GL_CreateContext(displayWindow);
        
#if !__APPLE__
        GLenum glewError = glewInit();
        if (glewError != GLEW_OK) {
            std::cout << "Error! Could not load OpenGL extensions!\n";
            std::cout << "\t" << glewGetErrorString(glewError) << "\n";
            return 0;
        }
#endif
        
        std::cout << "GL version:   " << glGetString(GL_VERSION) << "\n";
        std::cout << "GLSL version: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << "\n";
#if !__APPLE__
        std::cout << "GLEW version: " << glewGetString(GLEW_VERSION) << "\n";
#endif
        
        glViewport(0, 0, WIN_WIDTH, WIN_HEIGHT);
        glClearColor(0.5f, 0.6f, 0.7f, 1.0f);
        glFrontFace(GL_CCW);
        glEnable(GL_CULL_FACE);
        glCullFace(GL_BACK);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
        glShadeModel(GL_SMOOTH);
        
        if (!Initialize()) {
            std::cout << "Error! Could not initialize application.\n";
            exit(0);
        }
        Resize(WIN_WIDTH, WIN_HEIGHT);
        
        // For events
        Uint32 windowId = SDL_GetWindowID(displayWindow);
        
        // For delta time
        double prevTime = GetMilliseconds();
        double currentTime = GetMilliseconds();
        double deltaTime = 0.0;
        
        // For frame rate throttling
#if WIN32
        long nextGameTick = GetTickCount();
#else
        timeval time;
        gettimeofday(&time, NULL);
        long nextGameTick = (time.tv_sec * 1000) + (time.tv_usec / 1000);
#endif
        long sleepTime = 0;
        
        SDL_Event e;
        while (g_bAppRunning) {
            double timeOverride = 0.0;
            while (SDL_PollEvent(&e)) {
                if (e.window.windowID == windowId && e.type == SDL_WINDOWEVENT_RESIZED) {
                    glViewport(0, 0, e.window.data1, e.window.data2);
                    Resize(e.window.data1, e.window.data2);
                }
                else if (e.type == SDL_QUIT) {
                    g_bAppRunning = false;
                }
                else if (e.type == SDL_KEYUP) {
                    //printf( "Scancode: 0x%02X\n", e.key.keysym.scancode );
                    if (e.key.keysym.scancode == 0x29) {
                        g_bAppRunning = false;
                    }
                    if (e.key.keysym.scancode == 0x50) {
                        timeOverride = -0.0335149;
                        g_bManualTimestep = true;
                    }
                    else if (e.key.keysym.scancode == 0x4F) {
                        timeOverride = 0.0335149;
                        g_bManualTimestep = true;
                    }
                    else if (e.key.keysym.scancode == 0x2C) {
                        g_bManualTimestep = false;
                    }
                    
                }
                else if (e.type == SDL_KEYDOWN) {
                    OnKeyDown(e.key.keysym.scancode, deltaTime);
                }
            }
            
            currentTime = GetMilliseconds();
            deltaTime = double(currentTime - prevTime) * 0.001;
            prevTime = currentTime;
            
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            
            if (g_bManualTimestep) {
                if (!Update(timeOverride))
                    break;
            }
            else {
                if (!Update(deltaTime))
                    break;
            }
            Render();
            
            SDL_GL_SwapWindow(displayWindow);
            
            nextGameTick += SKIP_TICKS;
#if WIN32
            sleepTime = nextGameTick - GetTickCount();
#else
            gettimeofday(&time, NULL);
            sleepTime = nextGameTick - ((time.tv_sec * 1000) + (time.tv_usec / 1000));
#endif
            if (sleepTime > 0)
                SDL_Delay(Uint32(sleepTime));
        }
        
        Shutdown();
        
        SDL_GL_DeleteContext(openGlContext);
        SDL_DestroyWindow(displayWindow);
        SDL_Quit();
        
        return 0;
    }