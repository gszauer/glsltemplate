#include "PostProcess.h"
#include "Camera.h"
#include "Shader.h"
#include "Texture.h"
#include <assert.h>
#include <sstream>

void PostProcess::CreateFBO(int w, int h) {
    assert(m_nFramebufferId == 0);
    
    glGenFramebuffersEXT(1, &m_nFramebufferId);
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, m_nFramebufferId);
    
    glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, m_pOutputTexture->GetHandle(), 0);
    
    GLenum fboStatus = glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT);
    if(fboStatus != GL_FRAMEBUFFER_COMPLETE_EXT) {
        std::cout << "GL_FRAMEBUFFER_COMPLETE_EXT failed, CANNOT use FBO\n";
        PrintFBOStatus(fboStatus);
    }
    assert(fboStatus == GL_FRAMEBUFFER_COMPLETE_EXT);
    
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
}

void PostProcess::DestroyFBO() {
    if (m_nFramebufferId != 0)
        glDeleteFramebuffersEXT(1, &m_nFramebufferId);
    m_nFramebufferId = 0;
}

void PostProcess::DestroyVBO() {
    assert(m_nScreenSpaceVBO != 0);
    glDeleteBuffers(1, &m_nScreenSpaceVBO);
    m_nScreenSpaceVBO = 0;
}

void PostProcess::CreateVBO() {
    assert(m_nScreenSpaceVBO == 0);
    
    glm::vec3 verts[] = {
        glm::vec3(0.0f, 0.0f, 0.0f),
        glm::vec3(0.0f, 1.0f, 0.0f),
        glm::vec3(1.0f, 1.0f, 0.0f),
        glm::vec3(1.0f, 0.0f, 0.0f)};
    glm::vec2 texCoords[] = {
        glm::vec2(0.0f, 0.0f),
        glm::vec2(0.0f, 1.0f),
        glm::vec2(1.0f, 1.0f),
        glm::vec2(1.0f, 0.0f)};
    unsigned int indices[] = {2, 1, 0, 0, 3, 2};
    std::vector<float> vertexList; vertexList.reserve(30);
    for (int i = 0; i < 6; ++i) {
        vertexList.push_back(verts[indices[i]].x);
        vertexList.push_back(verts[indices[i]].y);
        vertexList.push_back(verts[indices[i]].z);
    }
    for (int i = 0; i < 6; ++i) {
        vertexList.push_back(texCoords[indices[i]].x);
        vertexList.push_back(texCoords[indices[i]].y);
    }
    
    assert(vertexList.size() > 0);
    glGenBuffers(1, &m_nScreenSpaceVBO);
    glBindBuffer(GL_ARRAY_BUFFER, m_nScreenSpaceVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * vertexList.size(), &vertexList[0], GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

PostProcess::PostProcess(int w, int h) {
    m_nScreenW = w;
    m_nScreenH = h;
    
    int u2 = 1;
    int v2 = 1;
    while(u2 < w) u2 *= 2;
    while(v2 < h) v2 *= 2;
    w = u2;
    h = v2;
    
    m_nTextureUniform = 0;
    m_nScaleUniform = 0;
    m_nMVPUniform = 0;
    m_nScreenSpaceVBO = 0;
    m_nBlitShader = 0;
    m_nFramebufferId = 0;
    m_nPositionAttrib = 0;
    m_nUvAttrib = 0;
    
    m_strInputUniform = "texture";
    m_strTypeUniform = "isDepth";
    m_strMvpUniform = "mvp";
    m_strClipPlanesUniform = "clip";
    m_strPositionAttribute = "position";
    m_StrUvAttribute = "uv";
    
    m_pInputTexture = 0;
    m_pOutputTexture = new Texture(w, h, Texture::RGBA_24);
    m_pShader = 0;
    CreateBlitShader();
    CreateFBO(w, h);
    CreateVBO();
}

void PostProcess::SetUniformNames(std::string& texture, std::string& type, std::string& mvp, std::string& clipPlanes) {
    m_strInputUniform = texture;
    m_strTypeUniform = type;
    m_strMvpUniform = mvp;
    m_strClipPlanesUniform = clipPlanes;
}

void PostProcess::SetAttributeName(std::string& position, std::string& uv) {
    m_strPositionAttribute = position;
    m_StrUvAttribute = uv;
}

PostProcess::PostProcess(int w, int h, Texture* inTex, Shader* shader) {
    m_nScreenW = w;
    m_nScreenH = h;
    
    int u2 = 1;
    int v2 = 1;
    while(u2 < w) u2 *= 2;
    while(v2 < h) v2 *= 2;
    w = u2;
    h = v2;
    
    m_strInputUniform = "texture";
    m_strTypeUniform = "isDepth";
    m_strMvpUniform = "mvp";
    m_strClipPlanesUniform = "clip";
    m_strPositionAttribute = "position";
    m_StrUvAttribute = "uv";
    
    m_nTextureUniform = 0;
    m_nScaleUniform = 0;
    m_nMVPUniform = 0;
    m_nScreenSpaceVBO = 0;
    m_nBlitShader = 0;
    m_nFramebufferId = 0;
    m_nPositionAttrib = 0;
    m_nUvAttrib = 0;
    
    m_pInputTexture = inTex;
    m_pOutputTexture = new Texture(w, h, Texture::RGBA_24);
    m_pShader = shader;
    CreateBlitShader();
    CreateFBO(w, h);
    CreateVBO();
}

PostProcess::~PostProcess() {
    DestroyVBO();
    DestroyFBO();
    if (m_pOutputTexture != 0)
        delete m_pOutputTexture;
    DestroyBlitShader();
}

void PostProcess::SetShader(Shader* shader) {
    m_pShader = shader;
}

void PostProcess::SetInput(Texture* texture) {
    m_pInputTexture = texture;
}

Texture* PostProcess::GetResult() {
    return m_pInputTexture;
}

Shader* PostProcess::GetShader() {
    return m_pShader;
}

void PostProcess::Resize(int w, int h) {
    m_nScreenW = w;
    m_nScreenH = h;
    
    int u2 = 1;
    int v2 = 1;
    while(u2 < w) u2 *= 2;
    while(v2 < h) v2 *= 2;
    w = u2;
    h = v2;
    
    DestroyFBO();
    if (m_pOutputTexture != 0)
        delete m_pOutputTexture;
    m_pOutputTexture = new Texture(w, h, Texture::RGBA_24);
    CreateFBO(w, h);
}

void PostProcess::SaveToPng() {
    assert(m_pOutputTexture != 0);
    
    long millis = ((long)(GetMilliseconds() * 0.001));
    std::cout << "Saving effect to png.\n";
    
    std::ostringstream oss;
    oss << GetEecutablePath() << millis << "_effect.png";
    
    if (GetEecutablePath() != "null")
        m_pOutputTexture->SavePNG(oss.str().c_str());
    else
        std::cout << "Saved image (" << millis << ") to: " << GetEecutablePath() << "\n";
}

void PostProcess::CreateBlitShader() {
    assert(m_nBlitShader == 0);
    const char* v_shader = "#version 120 \n\
                            uniform mat4 mvp;\
                            uniform vec2 scale;\
                            attribute vec2 uv;\
                            attribute vec3 position;\
                            varying vec2 texCoord;\
                            void main() {\
                                texCoord = uv * scale;\
                                gl_Position = mvp * vec4(position, 1.0);\
                            }";
    const char* f_shader = "#version 120 \n\
                            varying vec2 texCoord;\
                            uniform sampler2D texture;\
                            void main() {\
                                gl_FragColor = texture2D(texture, texCoord);\
                            }\0";
    
    GLuint vertexShader = CompileShader(v_shader, GL_VERTEX_SHADER);
    GLuint fragmentShader = CompileShader(f_shader, GL_FRAGMENT_SHADER);
    assert(vertexShader != -1 && fragmentShader != -1);
    
    // Make actual shader program
    GLint result;
    m_nBlitShader = glCreateProgram();
    glAttachShader(m_nBlitShader, vertexShader);
    glAttachShader(m_nBlitShader, fragmentShader);
    glLinkProgram(m_nBlitShader);
    glGetProgramiv(m_nBlitShader, GL_LINK_STATUS, &result);
    if (result == GL_FALSE) {
        std::cout << "Could not link shader program!\n";
        glGetProgramiv(m_nBlitShader, GL_INFO_LOG_LENGTH, &result);
        if (result > 0) {
            char* log = new char[result];
            glGetProgramInfoLog(m_nBlitShader, result, &result, log);
            std::cout << "\t: " << log << "\n";
            delete[] log;
        }
    }
    assert(result != GL_FALSE);
    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);
    
    // Populate uniforms
    glUseProgram(m_nBlitShader);
    m_nMVPUniform = glGetUniformLocation(m_nBlitShader, "mvp");
    m_nTextureUniform = glGetUniformLocation(m_nBlitShader, "texture");
    m_nUvAttrib = glGetAttribLocation(m_nBlitShader, "uv");
    m_nPositionAttrib = glGetAttribLocation(m_nBlitShader, "position");
    m_nScaleUniform = glGetUniformLocation(m_nBlitShader, "scale");
    glUseProgram(0);
}

void PostProcess::DestroyBlitShader() {
    if (m_nBlitShader != 0)
        glDeleteProgram(m_nBlitShader);
    m_nBlitShader = 0;
}

void PostProcess::BlitToScreen() {
    assert(m_pOutputTexture != 0);
    assert(m_nBlitShader != 0);
    assert(m_nScreenSpaceVBO != 0);
    
    glDisable(GL_DEPTH_TEST);
    glDepthMask(GL_FALSE);
    glUseProgram(m_nBlitShader);
    
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, m_pOutputTexture->GetHandle());
    float sx = m_nScreenW / m_pOutputTexture->GetWidth();
    float sy = m_nScreenH / m_pOutputTexture->GetHeight();
    glUniform2f(m_nScaleUniform, sx, sy);
    glUniform1i(m_nTextureUniform, 0);
    glUniformMatrix4fv(m_nMVPUniform, 1, GL_FALSE, glm::value_ptr(glm::ortho(0.0f, 1.0f, 0.0f, 1.0f, -1.0f, 1.0f)));
    
    glBindBuffer(GL_ARRAY_BUFFER, m_nScreenSpaceVBO);
    
    glEnableVertexAttribArray(m_nPositionAttrib);
    glEnableVertexAttribArray(m_nUvAttrib);
    
    glVertexAttribPointer(m_nPositionAttrib, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glVertexAttribPointer(m_nUvAttrib, 2, GL_FLOAT, GL_FALSE, 0, (GLvoid*)(6 * 3 * sizeof(float)));
    
    glDrawArrays(GL_TRIANGLES, 0, 6);
    
    glDisableVertexAttribArray(m_nPositionAttrib);
    glDisableVertexAttribArray(m_nUvAttrib);
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, 0);
    
    glUseProgram(0);
    glEnable(GL_DEPTH_TEST);
    glDepthMask(GL_TRUE);
}

void PostProcess::Bind() {
    assert(m_pInputTexture != 0);
    assert(m_pOutputTexture != 0);
    assert(m_pShader != 0);
    
    glBindFramebuffer(GL_FRAMEBUFFER, m_nFramebufferId);
    glDisable(GL_DEPTH_TEST);
    glDepthMask(GL_FALSE);
     m_pShader->Bind();
}

void PostProcess::Unbind() {
    glDepthMask(GL_TRUE);
    glEnable(GL_DEPTH_TEST);
    m_pShader->Unbind();
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void PostProcess::Execute(float _near, float _far) {
    assert(m_pInputTexture != 0);
    assert(m_pOutputTexture != 0);
    assert(m_pShader != 0);
    
    GLint viewport[4];
    glGetIntegerv(GL_VIEWPORT, viewport);
    glViewport(0, 0, m_pOutputTexture->GetWidth(), m_pOutputTexture->GetHeight());
    
    if (m_pShader->ContainsUniform(m_strInputUniform.c_str()))
        m_pShader->Texture(m_strInputUniform.c_str(), *m_pInputTexture);
    if (m_pShader->ContainsUniform(m_strTypeUniform.c_str()))
        m_pShader->Bool(m_strTypeUniform.c_str(), m_pInputTexture->GetFormat() == Texture::DEPTH);
    if (m_pShader->ContainsUniform(m_strMvpUniform.c_str()))
        m_pShader->Mat4(m_strMvpUniform.c_str(), glm::ortho(0.0f, 1.0f, 0.0f, 1.0f, -1.0f, 1.0f));
    if (m_pShader->ContainsUniform(m_strClipPlanesUniform.c_str()))
        m_pShader->Vec2(m_strClipPlanesUniform.c_str(), glm::vec2(_near, _far));
    
    GLuint positionAttrib = m_pShader->GetAttribLocation(m_strPositionAttribute.c_str());
    GLuint uvAttrib = m_pShader->GetAttribLocation(m_StrUvAttribute.c_str());
    
    if (m_pShader->ContainsAttribute(m_strPositionAttribute.c_str()) && m_pShader->ContainsAttribute(m_StrUvAttribute.c_str())) {
        glBindBuffer(GL_ARRAY_BUFFER, m_nScreenSpaceVBO);
        glEnableVertexAttribArray(positionAttrib);
        glEnableVertexAttribArray(uvAttrib);
        glVertexAttribPointer(positionAttrib, 3, GL_FLOAT, GL_FALSE, 0, 0);
        glVertexAttribPointer(uvAttrib, 2, GL_FLOAT, GL_FALSE, 0, (GLvoid*)(6 * 3 * sizeof(float)));
        glDrawArrays(GL_TRIANGLES, 0, 6);
        glDisableVertexAttribArray(positionAttrib);
        glDisableVertexAttribArray(uvAttrib);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }
    else {
        std::cout << "Could not find correct attributes!\n";
    }
    
    glViewport(viewport[0], viewport[1], viewport[2], viewport[3]);
}
