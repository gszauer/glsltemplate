#include "Application.h"
#include "OGLHeaders.h"
#include "Texture.h"
#include "PostProcess.h"
#include "Obj.h"
#include "Shader.h"
#include "Camera.h"
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <sstream>

int GetWidth() { return 800; }
int GetHeight() { return 600; }

glm::vec3 camPosition = glm::vec3(0.0f, 0.0f, -10.0f);
Obj* obj;
Shader* shader;
Camera* camera;
Shader* filterShader;
PostProcess* blurFilter;

bool g_bRenderFromCamer = false;

bool Initialize() {
    //std::cout << "Executable path: " << GetEecutablePath() << "\n";
    obj = new Obj("Assets/Cube.obj");
    shader = new Shader();
    shader->SetVertexShaderFromFile("Shaders/solid.vs");
    shader->SetFragmentShaderFromFile("Shaders/solid.fs");
    shader->Prepare();
    camera = new Camera(GetWidth(), GetHeight(), Camera::COLOR_DEPTH, 60.0f, float(GetWidth()) / float(GetHeight()), 0.01f, 1000.0f);
    filterShader = new Shader();
    filterShader->SetVertexShaderFromFile("Shaders/box.vs");
    filterShader->SetFragmentShaderFromFile("Shaders/box.fs");
    filterShader->Prepare();
    blurFilter = new PostProcess(GetWidth(), GetHeight(), camera->GetRenderTarget(), filterShader);
    PrintGLError();
    return  true;
}

void Resize(int w, int h) {
    camera->LookAt(camPosition, glm::vec3(0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
    camera->Perspective(60.0f, float(w) / float(h), 0.01f, 50.0f);
    camera->UpdateFrameBuffer(w, h);
    blurFilter->Resize(w, h);
    blurFilter->SetInput(camera->GetRenderTarget());
    PrintGLError();
}

void Render() {
    camera->Bind();
    
    glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearColor(0.5f, 0.6f, 0.7f, 1.0f);
    
    shader->Bind();
    
    shader->Mat4("view", camera->GetView());
    shader->Mat4("projection", camera->GetProjection());
    
    shader->Mat4("model", glm::translate(glm::vec3(0.0f, 0.0f, 1.0f)));
    shader->Vec3("color", glm::vec3(1.0f, 1.0f, 0.0f));
    obj->Render(shader->GetAttribLocation("position"));
    
    shader->Mat4("model", glm::translate(glm::vec3(1.0f, 1.0f, 3.0f)));
    shader->Vec3("color", glm::vec3(1.0f, 0.0f, 0.0f));
    obj->Render(shader->GetAttribLocation("position"));
    
    shader->Mat4("model", glm::translate(glm::vec3(-1.0f, -1.0f, 2.0f)));
    shader->Vec3("color", glm::vec3(0.0f, 1.0f, 0.0f));
    obj->Render(shader->GetAttribLocation("position"));
    
    
    shader->Unbind();
    camera->Unbind();
    
    if (g_bRenderFromCamer) {
        camera->BlitToScreen(false);
    }
    else {
        blurFilter->Bind();
        blurFilter->GetShader()->Vec2("invViewport", glm::vec2(
                1.0f / float(blurFilter->GetResult()->GetWidth()),
                1.0f / float(blurFilter->GetResult()->GetHeight())
            )
       );
        blurFilter->Execute();
        blurFilter->Unbind();
        blurFilter->BlitToScreen();
    }
}

bool Update(float dt) {
    PrintGLError();
    return true;
}

void Shutdown() {
    PrintGLError();
    delete blurFilter;
    delete filterShader;
    delete obj;
    delete shader;
    delete camera;
    PrintGLError();
}

void OnKeyDown(unsigned int key, float dt) {
    if (key == 44) {
        camera->SaveToPng();
        blurFilter->SaveToPng();
    }
    else if (key == 30) {
        g_bRenderFromCamer = true;
    }
    else if (key == 31) {
        g_bRenderFromCamer = false;
    }
    else if (key != 41) {
        std::cout << "Key pressed: " << key << "\n";
    }
}