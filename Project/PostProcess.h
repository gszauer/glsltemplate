#ifndef _H_POSTPROCESS_
#define _H_POSTPROCESS_

#include <vector>
#include <string>
#include "OGLHeaders.h"

class Camera;
class Shader;
class Texture;

class PostProcess {
protected:
    Texture*        m_pInputTexture;
    Texture*        m_pOutputTexture;
    Shader*         m_pShader;
    float           m_nScreenW;
    float           m_nScreenH;
    std::string     m_strInputUniform;
    std::string     m_strTypeUniform;
    std::string     m_strMvpUniform;
    std::string     m_strClipPlanesUniform;
    std::string     m_strPositionAttribute;
    std::string     m_StrUvAttribute;
    
    unsigned int    m_nBlitShader;
    unsigned int    m_nScreenSpaceVBO;
    unsigned int    m_nFramebufferId;
    unsigned int    m_nPositionAttrib;
    unsigned int    m_nTextureUniform;
    unsigned int    m_nMVPUniform;
    unsigned int    m_nUvAttrib;
    unsigned int    m_nScaleUniform;
protected:
    PostProcess();
    PostProcess(const PostProcess&);
    PostProcess& operator=(const PostProcess&);
    void CreateFBO(int w, int h);
    void CreateVBO();
    void DestroyFBO();
    void DestroyVBO();
    void CreateBlitShader();
    void DestroyBlitShader();
public:
    PostProcess(int w, int h);
    PostProcess(int w, int h, Texture* inTex, Shader* shader);
    ~PostProcess();
    
    void SetShader(Shader* shader);
    void SetInput(Texture* texture);
    
    Texture* GetResult();
    Shader* GetShader();
    void Resize(int w, int h);
    
    void SetUniformNames(std::string& texture, std::string& type, std::string& mvp, std::string& clipPlanes);
    void SetAttributeName(std::string& position, std::string& uv);
    
    void Bind();
    void Execute(float n = 0.0f, float f = 1.0f);
    void Unbind();
    
    void BlitToScreen();
    
    void SaveToPng();
};

#endif
