#ifndef _H_CAMERA_
#define _H_CAMERA_

#include "OGLHeaders.h"
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <vector>

class Texture;

class Camera {
public:
    enum RenderMode { COLOR_DEPTH, DEPTH };
protected:
    glm::vec3       m_vecPosition;
    glm::quat       m_quatRotation;
    glm::mat4       m_matView;
    glm::mat4       m_matProj;
    RenderMode      m_eRenderMode;
    Texture*        m_pRenderTarget;
    Texture*        m_pDepthTarget;
    float           m_nNearPlane;
    float           m_nFarPlane;
    float           m_nScreenW;
    float           m_nScreenH;
    unsigned int    m_nFramebufferId;
    unsigned int    m_nVBO;
    unsigned int    m_nBlitShader;
    unsigned int    m_nUvAttrib;
    unsigned int    m_nPositionAttrib;
    unsigned int    m_nScaleUniform;
    unsigned int    m_nTextureUniform;
    unsigned int    m_nClipPlanesUniform;
    unsigned int    m_nMVPUniform;
    unsigned int    m_nIsDepthUniform;
    unsigned int    m_nScreenSpaceVBO;
protected:
    Camera();
    Camera(const Camera&);
    Camera& operator=(const Camera&);
    void Destroy();
    void CreateRenderTarget(int w, int h);
    void CreateDepthTarget(int w, int h);
    void CreateFramebuffer();
    void CreateRenderVBO();
    void UpdateRenderVBO();
    std::vector<glm::vec3> GetWorldSpaceCorners();
    void InitBlitShader();
public:
    Camera(int screenWidth, int screenHeight, float fov, float aspect, float _near, float _far);
    Camera(int screenWidth, int screenHeight, RenderMode mode, float fov, float aspect, float _near, float _far);
    ~Camera();
    
    void Bind();
    void Unbind();
    
    void Perspective(float fov, float aspect, float _near, float _far);
    void LookAt(const glm::vec3& position, const glm::vec3& center);
    void LookAt(const glm::vec3& position, const glm::vec3& center, const glm::vec3& up);
    
    unsigned int GetFramebufferHandle();
    RenderMode GetRendermode();
    glm::vec3 GetPosition();
    glm::quat GetRotation();
    glm::mat4 GetView();
    glm::mat4 GetProjection();
    Texture* GetRenderTarget();
    Texture* GetDepthTarget();
    
    float GetNear();
    float GetFar();
    
    void SetRendermode(RenderMode mode);
    
    void Render(int position); // Draws frustum
    void BlitToScreen(bool depthBuffer = false);
    
    void UpdateFrameBuffer(int w, int h); // When the container window is resized
    
    void SaveToPng();
};

#endif