#ifndef _H_SHADER_
#define _H_SHADER_

#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <string>
#include <vector>
#include <map>

class Shader {
protected:
	unsigned int						m_nShaderHandle;
    unsigned int                        m_nVertHandle;
    unsigned int                        m_nFragHandle;
	char*								m_strCustomDefines;
	std::map<std::string, unsigned int>	m_mapUniformNameIndexMap;
	std::map<std::string, unsigned int>	m_mapAttributeNameIndexMap;
    unsigned int                        m_nUniformCount;
    unsigned int                        m_nAttributeCount;
	char*								m_strLastError;
    int                                 m_nLastBoundTextureIndex;
    std::string                         m_nVertName;
    std::string                         m_nFragName;
protected:
	Shader(const Shader&);
	Shader& operator=(const Shader&);
    void SetError(const char* error);
    void PopulateUniforms();
    void PopulateAttributes();
    int CompileShader_Internal(const char* shaderContent, unsigned int type);
public:
	Shader();
	Shader(const char* defines);
	Shader(const char* vertex, const char* fragment);
	Shader(const char* defines, const char* vertex, const char* fragment);
	~Shader();
	
	bool Prepare(); // Compiles and links
	void Bind();
	void Unbind();
	
	void SetDefines(const char* defines);
	void SetVertexShader(const char* shaderContent);
	void SetFragmentShader(const char* shaderContent);
	void SetVertexShaderFromFile(const char* path);
	void SetFragmentShaderFromFile(const char* path);
    
	unsigned int GetHandle();
    const char* GetLastError();
    
    // Get uniforms
    int GetUniformCount();
    unsigned int GetUniformLocation(const char* name);
    std::vector<std::string> GetUniformNames();
    bool ContainsUniform(const char* name);
    
    // Get attributes
    int GetAttribCount();
    unsigned int GetAttribLocation(const char* name);
    std::vector<std::string> GetAttribNames();
    bool ContainsAttribute(const char* name);
    
	// Set uniforms
    void Bool(const char* name, bool value);
    void Int(const char* name, int value);
    void Float(const char* name, float value);
    void Bvec2(const char* name, const glm::bvec2& value);
    void Bvec3(const char* name, const glm::bvec3& value);
    void Bvec4(const char* name, const glm::bvec4& value);
    void Ivec2(const char* name, const glm::ivec2& value);
    void Ivec3(const char* name, const glm::ivec3& value);
    void Ivec4(const char* name, const glm::ivec4& value);
    void Vec2(const char* name, const glm::vec2& value);
    void Vec3(const char* name, const glm::vec3& value);
    void Vec4(const char* name, const glm::vec4& value);
    void Mat2(const char* name, const glm::mat2& value);
    void Mat3(const char* name, const glm::mat3& value);
    void Mat4(const char* name, const glm::mat4& value);
    void Texture(const char* name, class Texture& value);
};

#endif