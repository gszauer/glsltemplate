#version 120

varying vec2 texCoord;

uniform sampler2D texture;
uniform vec2 invViewport;

void main() {
    vec2 up = vec2(0.0, 1.0) * invViewport.y;
    vec2 down = vec2(0.0,-1.0) * invViewport.y;
    vec2 left = vec2(1.0, 0.0) * invViewport.x;
    vec2 right = vec2(-1.0, 0.0) * invViewport.x;
    
    vec4 c = texture2D(texture, texCoord);
    
    c += texture2D(texture, texCoord + left) * 0.5;
    c += texture2D(texture, texCoord + right) * 0.5;
    c += texture2D(texture, texCoord + up) * 0.5;
    c += texture2D(texture, texCoord + down) * 0.5;
    
	gl_FragColor = c;
}