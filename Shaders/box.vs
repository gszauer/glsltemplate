#version 120 

attribute vec2 uv;
attribute vec3 position;

uniform mat4 mvp;

varying vec2 texCoord;

void main() {
	texCoord = uv;
	gl_Position = mvp * vec4(position, 1.0);
}